<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Bird", inversedBy="categories")
     */
    private $Birds;

    public function __construct()
    {
        $this->Birds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Bird[]
     */
    public function getBirds(): Collection
    {
        return $this->Birds;
    }

    public function addBird(Bird $bird): self
    {
        if (!$this->Birds->contains($bird)) {
            $this->Birds[] = $bird;
        }

        return $this;
    }

    public function removeBird(Bird $bird): self
    {
        if ($this->Birds->contains($bird)) {
            $this->Birds->removeElement($bird);
        }

        return $this;
    }
}
