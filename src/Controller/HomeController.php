<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Utilisateur;


class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function default()
    {
        return $this->render('base.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function show(Utilisateur $user)
    {
        return $this->render('show.html.twig', [
            'user' => $user,
        ]);
    }
}
