var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .addEntry('app', './assets/js/app.2a684fd4.js')
    .enableVueLoader()

    .enableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();